
var basePath = '';
$(window).on('resize scroll', function () {
	var scroll = $(window).scrollTop();
    if(scroll > 0){
        $('.header_mid').css('height', '45px');
    }
    else{
        $('.header_mid').css('height', '70px');
    }
    if (scroll > 700){
        $('#slide-top').show()
    }else{
        $('#slide-top').hide()
    }
});

$('#slide-top').click(function(){
    window.scrollTo(0,0);
});

$('#big-search-container .close-icon').click(function(){
    $('#big-search-container').hide();
});

$('.header-search-icon').click(function(){
    $('#big-search-container').show();
});

function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
    document.getElementById("mySidenav").style.paddingLeft = "20px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("mySidenav").style.paddingLeft = "0px";
}

function openNav2() {
    document.getElementById("mySidenav2").style.width = "100%";
    document.getElementById("mySidenav2").style.paddingLeft = "20px";
}

function closeNav2() {
    document.getElementById("mySidenav2").style.width = "0px";
    document.getElementById("mySidenav2").style.paddingLeft = "0px";
}